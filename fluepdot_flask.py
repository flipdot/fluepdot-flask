import fluepdoter as fd
import time
from flask import Flask, render_template, redirect, request, url_for

app = Flask(__name__)
fd.set_url("http://192.168.6.104")

@app.get('/')
def index():
    return render_template('index.html')

@app.post('/text')
def text():
    fd.post_text(f"{request.form['name']}")
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run()
