# Fluepdot Flask

Little Flask webapp for fluepdot text input.

The `fluepdoter.py` is this: https://github.com/KS-HTK/python-fluepdot

## Howto

1. Set IP address in `fluepdot_flask.py`
2. `python3 fluepdot_flask.py`
3. profit
